/**
 * @file
 * Full Page Photo Gallery JS taken from http://tympanus.net/codrops/2010/09/08/full-page-image-gallery/ .
 *
 * Modified for Drupal Photo Gallery, added features, changed a lot.
 * @Author: Rick Tilley
 * @Date: 1/1/2015
 */

(function($) {
  // Show full screen gallery toggle switch.
  $('.fp_switch').html('<input id="cmn-toggle-1" class="cmn-toggle cmn-toggle-round-flat" type="checkbox" name="Full Screen Toggle"><label for="cmn-toggle-1"></label>');
  $('#fp_switchtxt').css('display', 'inline-block');
  $('#cmn-toggle-1').change(function () {
    if ($(this).is(':checked')) {
      fp_noJS_css();
      fp_init();
      fp_loadinit();
      $('#fp_switchtxt b').text('Enabled');
    }
    else {
      $('#fp_thumbtoggle').stop().animate({'bottom':'-50px'},500);
      $('#fp_next').stop().animate({'right':'-50px'},500);
      $('#fp_prev').stop().animate({'left':'-50px'},500);
      $("#fp-slider").slider("destroy");
      $("#fp-slider-value").text("");
      if (previous_pager != null || next_pager != null) {
        $('.item-list ul.pager').show();
        if (previous_pager != null) {
          $('.fp-content:first').remove();
        }
        if (next_pager != null) {
          $('.fp-content:last').remove();
        }
        previous_pager = next_pager = null;
      }
      fp_noJS_basiccss();
      fp_unregisterlisteners();
      $('#fp_switchtxt b').text('Off');
    }
  });

  window.addEventListener('load', function() {
    $('#cmn-toggle-1').delay(400).prop('checked', true).trigger('change');
  });
  function fp_noJS_css() {
    // Modify CSS to correspond, currently in noJS mode.
    $('#fp_gallery').removeClass('noJS-fp');
    $('#outer_container').removeClass('noJS-fp');
    $('#thumbScroller').removeClass('noJS-fp');
    $('#thumbScroller .fp-content').removeClass('noJS-fp');
    $('#thumbScroller .thumb-content').attr('style', 'margin: 0');
  }

  function fp_noJS_basiccss() {
    // Modify CSS to correspond, currently in noJS mode.
    $('#outer_container').attr('style', '');
    $('#thumbScroller').attr('style', '');
    $('#thumbScroller .thumb-content').attr('style', '');
    $('.fp_thumbtoggle').attr('style', '');
    $('#thumbScroller img').attr('style', '');
    $('#thumbScroller .thumb-content').attr('style', '');
    $('#fp_gallery').addClass('noJS-fp');
    $('#outer_container').addClass('noJS-fp');
    $('#thumbScroller').addClass('noJS-fp');
    $('#thumbScroller .fp-content').addClass('noJS-fp');
  }

  function fp_unregisterlisteners() {
    $('<img/>').unbind('load', fp_thumbload);
    $('body').unbind('keyup', fp_exitimage);
    $('#fp_gallery b').unbind();
    $('#fp_thumbtoggle').unbind();
    $('#fp_next').unbind();
    $('#fp_prev').unbind();
    $('#thumbScroller').find('.fp-content').unbind();
    $('<img class="fp_preview"/>').unbind();
    $('#thumbScroller').unbind();
    $('#thumbScroller .image-style-gallery-thumbnail').unbind();
    $('#fp_verbaigeouter p.fp_instruct u').unbind();
    window.removeEventListener('resize', fp_resize);
    fp_thumbkey = null;
  }

  function fp_init() {
    // Current thumb's index being viewed.
    var current   = -1;
    // Cache some elements.
    var $btn_thumbs = $('#fp_thumbtoggle');
    var $loader = $('#fp_loading');
    var $btn_next = $('#fp_next');
    var $btn_prev = $('#fp_prev');
    var $thumbScroller = $('#thumbScroller');
    modpath = "/" + $('#fp_modpath').html();
    last_fp_exit = $('#fp_exit h2').html();
    fp_exittime = 7000;
    pager_clicked = 0;
    // Collapsible instructions.
    $('#fp_verbaigeouter p.fp_instruct u').click(function() {
      var inst_header = $(this);
      var inst_content = $('#fp_instructions');
      var icontent = "<div id='fp_instructionsCB'><span class='close'>X</span>" + inst_content.html() + "</div>";
      instructionbox(icontent);
    });
    // Get pager buttons.
    if ($(".item-list ul.pager").length > 0) {
      if ($(".item-list ul.pager li.pager-previous a").length > 0) {
        previous_pager = $(".item-list ul.pager li.pager-previous a").attr('href');
      }
      else {
        previous_pager = null;
      }
      if ($(".item-list ul.pager li.pager-next a").length > 0) {
        next_pager = $(".item-list ul.pager li.pager-next a").attr('href');
      }
      else {
        next_pager = null;
      }
    }
    else  {
      previous_pager = next_pager = null;
    }
    // Create page widget - slider.
    $("#fp-slider").slider({min: 1.0, max: 60.0, step: 0.2, value: 10.0,
      change: function(event, ui) {
        slidertime = ui.value;
      },
      slide: function(event, ui) {
        var sectext = " seconds";
        if (ui.value == 1) {
          sectext = " second";
        }
        $("#fp-slider-value").text("Slideshow speed: " + ui.value + sectext + ". (On hover)");
      },
      stop: function(event, ui) {
        $("#fp-slider a").blur();
      }
    });
    slidertime = 10.0;
    $("#fp-slider-value").text("Slideshow speed: " + slidertime + " seconds. (On hover)");
  /*  $('.view-photo-gallery .view-filters').remove(); */
    // Previous pager button.
    function pagerbutton(direction, spot) {
      var pageimg = spot.clone();
      pageimg.find('a').attr('href', direction);
      pageimg.find('img').attr('alt', direction + ' page').attr('title', direction + ' page');
      pageimg.find('img').attr('src', modpath + '/res/' + direction + '-arrow-fpgallery.png');
      if (direction == 'previous') {
        spot.before(pageimg);
      }
      else {
        spot.after(pageimg);
      }
    }
    if (previous_pager != null || next_pager != null) {
      var ghosthtml = '';
      if (previous_pager != null) {
        pagerbutton('previous', $('.fp-content:first'));
      }
      // Next pager button.
      if (next_pager != null) {
        pagerbutton('next', $('.fp-content:last'));
      }
      $('.item-list ul.pager').hide();
    }
    // Total number of thumbs.
    var nmb_thumbs = $thumbScroller.find('.fp-content').length;
    hideCaption();
    var cnt_thumbs = 0;
    fp_thumbkey = {};
    // Preload thumbs.
    for(var i = 0; i < nmb_thumbs; ++i){
     var $thumb = $thumbScroller.find('.fp-content:nth-child(' + parseInt(i + 1) + ')');
     // Store links in associative array.
     var $thumblink = $thumb.find('img').attr('src');
     var $imglink = $thumb.find('a').attr('href');
     fp_thumbkey[$thumblink] = $imglink;
     // Done storing info.
     $('<img/>').load(fp_thumbload = function(){
      ++cnt_thumbs;
      if (cnt_thumbs == 1) {
        // Display the thumbs on the bottom of the page.
        showThumbsinline(2000);
        hideNav();
      }
     }).attr('src', $thumb.find('img').attr('src'));
    }
    // Allow escape key to get out of full screen.
    $('body').keyup(fp_exitimage = function(e) {
      var keycode = (e.keyCode ? e.keyCode : e.which);
      switch (keycode) {
        case 27:
          // Esc key.
          $('#fp_gallery b').css('display', 'none');
          $('#fp_gallery b img.fp_preview').attr('src', modpath + '/res/transdot.png');
          hideNav();
          hideCaption();
          stopslideshow();
          break;

        case 37:
          // Left.
          e.stopPropagation();
          stopslideshow();
          showPrev();
          break;

        case 39:
          // Right.
          e.stopPropagation();
          stopslideshow();
          showNext();
          break;

        default:
          return;
      }
      e.preventDefault();
    });

    $('#fp_gallery b').click(function (e) {
      if ($(e.target).hasClass('fp_preview')) {
        stopslideshow();
        return;
      }
      $('#fp_gallery b').css('display', 'none');
      $('#fp_gallery b img.fp_preview').attr('src', modpath + '/res/transdot.png');
      hideNav();
      showThumbsinline();
      hideCaption();
      stopslideshow();
    });
    /* makeScrollable(); */
    dip_topage = function(loc, hloc) {
      var tomsg = "Loading " + loc + " page of photos";
      $('#fp_exit h2').html(tomsg);
      fp_exittime = 50000;
      pager_clicked = 1;
      $('#fp_gallery b').css('opacity', '0');
      $('#fp_exit h2').css('background-color', 'rgba(67, 81, 50, 1)');
      $('#fp_exit h2').css('margin', '15% auto');
      location.href = hloc;
      $('#fp_exit').stop().show().fadeIn(1);
    }
    // Clicking on a thumb...
    $thumbScroller.find('.fp-content').bind('click', function(e) {
     var $content = $(this);
     var $elem = $content.find('img');
     var $currImage = $('#fp_gallery').children('b').children('img:first');
     // Check to see if next or previous pager thumbs clicked.
     pager_clicked = 0;
     if (fp_thumbkey[$elem.attr('src')] == 'previous') {
       dip_topage('previous', previous_pager);
     }
     else if (fp_thumbkey[$elem.attr('src')] == 'next') {
       dip_topage('next', next_pager);
     }
     $('#fp_gallery b').css('display', 'inherit');
     $currImage.fadeOut(500);
     // Show exit instructions a few times.
     var fadetime = (fp_exittime > 5000) ? 3000 : 500;
     var delaytime = (fp_exittime > 3999) ? fp_exittime / 2 : 1000;
     if (fp_exittime > 1999) {
       $('#fp_exit').stop().show().delay(delaytime).fadeOut(fadetime);
       // Reduce exit message show time.
       fp_exittime = fp_exittime - 1000;
     }
     if (pager_clicked == 1) {
       e.preventDefault();
       return;
     }
     // Keep track of the current clicked thumb.
     // It will be used for the navigation arrows.
     current = $content.index() + 1;
     // Get the positions of the clicked thumb.
     var pos_left = $elem.offset().left;
     var pos_top = $elem.offset().top;
     // Clone the thumb and place.
     // The clone on the top of it.
     var $clone = $elem.clone()
     .addClass('clone')
     .css({
      'position':'fixed',
      'left': pos_left + 'px',
      'top': pos_top + 'px',
      'border': 'none',
     }).insertAfter($('BODY'));

     var windowW = $(window).width();
     var windowH = $(window).height();

     // Animate the clone to the center of the page.
     $clone.stop()
     .animate({
      'left': windowW / 2 + 'px',
      'top': windowH / 2 + 'px',
      'margin-left' : -$clone.width() / 2 - 5 + 'px',
      'margin-top': -$clone.height() / 2 - 5 + 'px'
     }, 500,
     function(){
      var $theClone = $(this);
      var ratio = $clone.width() / 120;
      var final_w = 400 * ratio;
      $loader.show();
      // Expand the clone when large image is loaded.
      $('<img class="fp_preview"/>').load(function(){
       var $newimg = $(this);
       var $currImage = $('#fp_gallery').children('b').children('img:first');
       $newimg.fadeIn(2000);
       if ($newimg.get(0) != $currImage.get(0)) {
         $newimg.insertBefore($currImage);
         $currImage.remove();
       }
       $loader.hide();
       // Expand clone.
       $theClone.animate({
         'opacity'  : 0,
         'top'   : windowH / 2 + 'px',
         'left'   : windowW / 2 + 'px',
         'margin-top' : '-200px',
         'margin-left' : -final_w / 2 + 'px',
         'width'   : final_w + 'px',
         'height'  : '400px'
       }, 1000, function() {$(this).remove();});
       /* Now we have two large images on the page
          fadeOut the old one so that the new one gets shown
          EDIT! removed and moved up
         show the navigation arrows
       */
       $('#fp_caption h3').html($newimg.attr('alt'));
       if ($newimg.attr('src') != modpath + '/res/transdot.png') {
         showNav();
         showCaption();
       }
      }).attr('src',fp_thumbkey[$elem.attr('src')]).attr('alt', $elem.attr('alt'));
     });
     // Hide the thumbs container.
     hideThumbs();
     e.preventDefault();
    });
    /* Clicking on the "show thumbs"
       Displays the thumbs container and hides
       The navigation arrows
    */
    $btn_thumbs.bind('click', function() {
      showThumbs(500);
      hideNav();
      hideCaption();
    });

    function showCaption() {
      $('#fp_caption').stop().animate({'bottom':'10%'}, 500);
    }

    function hideCaption() {
      $('#fp_caption').stop().animate({'bottom':'-10%'}, 500);
    }

    function hideThumbs(){
     $('#outer_container').stop().animate({'bottom':'-170px'}, 500);
     $('#thumbScroller').stop().animate({'bottom':'-170px'}, 500);
     if (pager_clicked != 1) {
       showThumbsBtn();
     }
    }

    function showThumbsinline(speed) {
      // First resize for mobile devices to recalibrate.
      fp_resize();
      var xoff = $('.thumbrowspace').offset().top;
      var x = $(window).scrollTop() + $(window).height() - xoff - 120 - 2;
      if (x < 0) {
        x = 0;
      }
      var xpos = x + "px";
      $('#outer_container').stop().animate({'bottom':xpos}, speed);
      $('#thumbScroller').stop().animate({'bottom':xpos}, speed);
      hideThumbsBtn();
      var n = document.createTextNode(' ');
    }

    function showThumbs(speed){
      // First resize for mobile devices to recalibrate.
      fp_resize();
      $('#outer_container').stop().animate({'bottom':'0px'}, speed);
      $('#thumbScroller').stop().animate({'bottom':'0px'}, speed);
      hideThumbsBtn();
      var n = document.createTextNode(' ');
    }

    function hideThumbsBtn(){
     $btn_thumbs.stop().animate({'bottom':'-50px'}, 500);
    }

    function showThumbsBtn(){
     $btn_thumbs.stop().animate({'bottom':'0px'}, 500);
    }

    function hideNav(){
     $btn_next.stop().animate({'right':'-50px'}, 500);
     $btn_prev.stop().animate({'left':'-50px'}, 500);
    }

    function showNav(){
     $btn_next.stop().animate({'right':'0px'}, 500);
     $btn_prev.stop().animate({'left':'0px'}, 500);
    }

    // Events for navigating through the set of images.
    $btn_next.bind('click', function(e) {
      e.stopPropagation();
      stopslideshow();
      showNext();
    });
    $btn_prev.bind('click', function(e) {
      e.stopPropagation();
      stopslideshow();
      showPrev();
    });

    function stopslideshow() {
      slider = 0;
      if (typeof timer !== 'undefined') {
        clearTimeout(timer);
      }
    }
    function startslideshow() {
      slider = 1;
      var deltime = Math.round(slidertime * 1000) - 5000;
      if (deltime > 999) {
        timer = setTimeout(showNext, deltime);
      }
      else {
        showNext();
      }
    }
    function prevstartslideshow() {
      slider = 1;
      var deltime = Math.round(slidertime * 1000) - 5000;
      if (deltime > 999) {
        timer = setTimeout(showPrev, deltime);
      }
      else {
        showPrev();
      }
    }
    var nextconfig = {
        over: startslideshow,
        timeout: 100,
        interval: 5000,
        out: stopslideshow
        };
   $btn_next.hoverIntent(nextconfig);
   var prevconfig = {
       over: prevstartslideshow,
       timeout: 100,
       interval: 5000,
       out: stopslideshow
       };
   $btn_prev.hoverIntent(prevconfig);

   // Show 'going to start/end of photos' message when repeating.
   function showFlipPage(gotomsg) {
     var fp_exitmsg = $('#fp_exit h2').html();
     $('#fp_exit h2').html(gotomsg);
     $('#fp_exit').stop().fadeIn(1);
     setTimeout(function() {
       $('#fp_exit').stop().fadeOut(1000);
       setTimeout(function() {
         var fp_exitmsg = $('#fp_exit h2').html();
         if (fp_exitmsg.indexOf("Loading" < 0)) {
           $('#fp_exit h2').html(last_fp_exit);
         }
       }, 1500);
     }, 4000);
   }
    /* The aim is to load the new image,
       place it before the old one and fadeOut the old one
       we use the current variable to keep track which
       image comes next / before
    */
    function showNext(){
      $('#fp_gallery b').css('display', 'inherit');
     ++current;
     var $e_next = $thumbScroller.find('.fp-content:nth-child(' + current + ')');
     if($e_next.length == 0 || $e_next.find('a').attr('href') == 'next') {
       showFlipPage('End of page, returning to first photo');
       current = 1;
       $e_next = $thumbScroller.find('.fp-content:nth-child(' + current + ')');
       if ($e_next.find('a').attr('href') == 'previous') {
         current = 2;
         $e_next = $thumbScroller.find('.fp-content:nth-child(' + current + ')');
       }
     }
     $loader.show();
     $('<img class="fp_preview"/>').load(function(){
      var $newimg = $(this);
      var $currImage = $('#fp_gallery').children('b').children('img:first');
      $currImage.fadeOut(500);
      $newimg.fadeIn(2000);
      if ($newimg.get(0) != $currImage.get(0)) {
        $newimg.insertBefore($currImage);
        $currImage.remove();
      }
      $loader.hide();
      $('#fp_caption h3').html($newimg.attr('alt'));
     }).attr('src',fp_thumbkey[$e_next.find('img').attr('src')]).attr('alt', $e_next.find('img').attr('alt'));
     if (slider == 1) {
       timer = setTimeout(showNext, Math.round(slidertime * 1000));
     }
    }

    function showPrev(){
      $('#fp_gallery b').css('display', 'inherit');
     --current;
     var $e_next = $thumbScroller.find('.fp-content:nth-child(' + current + ')');
     if($e_next.length == 0 || $e_next.find('a').attr('href') == 'previous'){
       showFlipPage('Beginning of page, jumping to last photo');
       current = nmb_thumbs;
       $e_next = $thumbScroller.find('.fp-content:nth-child(' + current + ')');
       if ($e_next.find('a').attr('href') == 'next') {
         current--;
         $e_next = $thumbScroller.find('.fp-content:nth-child(' + current + ')');
       }
     }
     $loader.show();
     $('<img class="fp_preview"/>').load(function(){
      var $newimg = $(this);
      var $currImage = $('#fp_gallery').children('b').children('img:first');
      $currImage.fadeOut(500,function(){
        $(this).attr(src, modpath + '/res/transdot.png');
       });
      $newimg.fadeIn(2000);
      if ($newimg.get(0) != $currImage.get(0)) {
        $newimg.insertBefore($currImage);
        $currImage.remove();
      }
      $loader.hide();
      $('#fp_caption h3').html($newimg.attr('alt'));
     }).attr('src',fp_thumbkey[$e_next.find('img').attr('src')]).attr('alt', $e_next.find('img').attr('alt'));
     if (slider == 1) {
       timer = setTimeout(showPrev, Math.round(slidertime * 1000));
     }
    }
    function instructionbox(icontent) {
      // First hide all navigation elements.
      pager_clicked = 1;
      hideThumbs();
      hideThumbsBtn();
      $('#fp_gallery b').css('display', 'inherit').css('opacity', '0.9');
      $('#fp_gallery').append(icontent)
      $('#fp_instructionsCB').center().hide().fadeIn(1000);
      $('#fp_instructionsCB span.close').click(closeinstructionsbox = function(e) {
        instructionboxclose(e);
      });
      $('#fp_gallery b').click(closeinstructions = function(e) {
        instructionboxclose(e);
      });
    }
    function instructionboxclose(e) {
      $('#fp_instructionsCB').remove();
      $('#fp_gallery b').css('display', 'none').css('opacity', '1');
      $('#fp_gallery b').unbind("click", closeinstructions);
      $('#fp_gallery b').unbind("click", closeinstructionsbox);
      pager_clicked = 0;
      showThumbs();
      e.preventDefault();
    }
    /*
    function makeScrollable(){
     $(document).bind('mousemove',function(e){
      var top = (e.pageY - $(document).scrollTop()/2) ;
      $(document).scrollTop(top);
                    });
    }
    */
   }
   /* other file's function */
  function fp_loadinit() {
    sliderLeft = $('#thumbScroller .thumb-content').position().left;
    padding = $('#outer_container').css('paddingRight').replace("px", "");
    sliderWidth = $(window).width() - padding;
    $('#thumbScroller').css('width', '100%');
    totalContent = 0;
    $('#thumbScroller .fp-content').each(function() {
     totalContent += $(this).outerWidth() + 2;
    });
    totalContent += 60;
    $('#thumbScroller .thumb-content').css('width', totalContent);
    $('#thumbScroller').mousemove(function(e){
     if ($('#thumbScroller .thumb-content').width() > sliderWidth){
      var mouseCoords = (e.pageX - this.offsetLeft) - 15;
      var mousePercentX = mouseCoords / sliderWidth;
      var destX = -(((totalContent - (sliderWidth)) - sliderWidth) * (mousePercentX)) + 15;
      var thePosA = mouseCoords - destX;
      var thePosB = destX - mouseCoords;
      var animSpeed = 600;
      var easeType = 'easeOutCirc';
      if(mouseCoords == destX){
       $('#thumbScroller .thumb-content').stop();
      }
      else if(mouseCoords > destX){
       $('#thumbScroller .thumb-content').stop().animate({left: -thePosA}, animSpeed, easeType);
      }
      else if(mouseCoords < destX){
       $('#thumbScroller .thumb-content').stop().animate({left: thePosB}, animSpeed, easeType);
      }
     }
    });
    $('#thumbScroller .image-style-gallery-thumbnail').each(function () {
     $(this).fadeTo(fadeSpeed, 0.6);
    });
    var fadeSpeed = 200;
    $('#thumbScroller .image-style-gallery-thumbnail').hover(
    function(){
      // Mouse over.
     $(this).fadeTo(fadeSpeed, 1);
    },
    function(){
      // Mouse out.
     $(this).fadeTo(fadeSpeed, 0.6);
    }
   );

   window.addEventListener('resize', fp_resize = function() {
      sliderWidth = $(window).width() - padding;
      $('#thumbScroller .thumb-content').stop().animate({left: sliderLeft}, 400,'easeOutCirc');
      $('#thumbScroller').css('width',sliderWidth);
     })
   }

  jQuery.fn.center = function () {
    this.css("position","fixed");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
    return this;
  }

})(jQuery);
