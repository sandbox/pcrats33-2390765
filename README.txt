Full Page Photo Gallery (FP Photo Gallery) for Drupal 7.x

INTRODUCTION
------------
This module is to provide a photo gallery to showcase website photos.  It uses
a JavaScript implementation of:
  http://tympanus.net/codrops/2010/09/08/full-page-image-gallery/
This sample gallery has been modified to 'fit' into the Drupal framework using
views, and new features have been added to it, such as a slideshow, a pager,
and arrow key functionality.  The gallery involves a row of thumbnails to
select photos with, and the photos will show full screen in the browser.

This module requires views and some jQuery libraries.

REQUIREMENTS
------------
This gallery requires a bit of jQuery libraries that are not included in Drupal
core.  Once you download and install all module dependencies, you should have
everything that you need. You will also need to download jQuery UI - v1.10.4
or a later version, and place it in the module's /js/ directory.  This module
will not work without that library.  Unfortunately even though jQuery-UI is
included in Drupal 7.x core, the version is missing quite a bit.  Visit
jqueryui.com and download v1.10.4 or the latest version.  Be sure to rename
the file jquery-ui.js and place it in the /js/ directory as so:
fp_photo_gallery/js/jquery-ui.js

You will also need the following modules:
drupal.org/project/views
drupal.org/project/jquery_update
drupal.org/project/jqeasing -- Be sure to follow its instructions
correctly when installing and verify it is installed correctly.
drupal.org/project/hoverintent

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
CONFIGURATION
-------------
** Step 1 ** 
Create an image style, or some way for views to output a resized version of the
image at 80 x 120px.

** Step 2 **
Create a view that pulls in the images you want, reformat the view output as:
<div><a href="[uri]">[field_photo]</a></div>
Where [uri] is the full URL to the full sized photo, and [field_photo] is an img
tag sized at 80 x 120px.  The gallery is designed to work on images with alt
tags, but should function properly even if alt tags are empty.  

You can use view relationships on your photo field to access the image's
URL.  Also, use minimal style settings on the field that displays, so under
style settings, check 'Customize field HTML' and set the drop-down to '- None -'
Also uncheck the box that says Add default classes.

** Step 3 **
Choose view FORMAT and select FP Photo Gallery as your view format.

** Step 4 is optional **
Edit the module's /views/fp-photo-gallery-view.tpl.php to customize the layout
of the page.  Many of the divs are required, so don't change anything if you
don't understand the consequence.  There is a dummy button on the template
that you will probably want to update or remove:
    <div id="fp_verbaige">
    <a href="/"><div class="gallery_fplink">
    Problems with the gallery?<br />This default link will send you home</div>
    </a></div>
Also below the instructions is lots of room for text or an image.  I currently
put a blank div with a fixed height to fill in the blank space, but you can
customize this however you like:
    <!--  put an image here if you like, just to push the thumbnails down -->
    <div class="fp_emptyspace"></div>
    <!--  the div fp_emptyspace can be removed -->
Everything else can be left the same, anything below <div id="fp_themepath">
should not be touched.

** A Note **
Because this uses views, you can use contextual filters, exposed filters, 
a pager, and other nice views customizations.  I hope you enjoy this module
and feel free to contact me with comments or concerns.

TROUBLESHOOTING
---------------
If the Loading button never slides right and turns green, or the thumbnail
images do not slide up from underneath the browser window, there is probably
an issue with the JavaScript.  Ensure that all the required JavaScript libraries
are loading.  This module requires jQuery, jQuery-UI, HoverIntent, jQ-Easing,
and Colorbox JavaScript libraries.  Use Firebug and check the console for
errors.  I tried to make this as non-custom as possible, but if a div is missing
from the .tpl file, or some other javascript issue arrises, it is possible it
will crash the main routine.  Feel free to contact the author with questions on
these types of conflicts.

MAINTAINERS
-----------
Current Maintainer:
 * Rick Tilley (pcrats33) - https://www.drupal.org/u/pcrats33
