<?php
/**
 * @file
 * Contains the fp_photo_gallery style plugin.
 */

/**
 * Style plugin to render each item as a row in a responsive table.
 */
class FPPhotoGalleryStylePlugin extends views_plugin_style_table {
  /**
   * Provide default options.
   */
  public function optionDefinition() {
    $options = parent::option_definition();
    $options['fp_photo_gallery'] = array('default' => array());
    return $options;
  }

  /**
   * Adds fp_photo_gallery configuration form options.
   */
  public function optionsForm(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    /* Check if fields have been added.  Table style plugin will set
       error_markup if fields are not added.
       @see views_plugin_style_table::options_form()
    */
    if (isset($form['error_markup'])) {
      return;
    }
    $form['fp_photo_gallery'] = array(
      '#markup' => t('Everything is as it needs to be.'),
    );
  }
}
