<?php

/**
 * @file
 * FP photo gallery views hooks.
 */

/**
 * Implements hook_views_plugins().
 */
function fp_photo_gallery_views_plugins() {
  $path = drupal_get_path('module', 'fp_photo_gallery') . '/views';
  return array(
    'module' => 'fp_photo_gallery',
    'style' => array(
      'fp_photo_gallery' => array(
        'title' => t('FP Photo Gallery'),
        'help' => t('Full Page Photo Gallery.'),
        'path' => $path,
        'theme path' => $path,
        'handler' => 'FPPhotoGalleryStylePlugin',
        'parent' => 'unformatted_list',
        'theme' => 'fp_photo_gallery_view',
        'uses row plugin' => FALSE,
        'uses row class' => FALSE,
        'uses fields' => TRUE,
        'uses options' => FALSE,
        'type' => 'normal',
      ),
    ),
  );
}
