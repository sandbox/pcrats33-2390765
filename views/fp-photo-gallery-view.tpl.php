<?php
/**
 * @file
 * Template to display FP Gallery Views.
 *
 * This is the place that defines the output of view using this gallery module.
 * This page can be customized within #fp_verbaigeouter, otherwise I highly
 * recommend against editing below this outer div.  Also the slider and
 * instruction divs are modified by the JavaScript, and removing them will
 * cause errors, possibly crashing the script.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<div id="fp_verbaigeouter">
  <div id="fp_switchtxt" class="fp_switchtxt">
    <b>
      <span class="helper"></span>
      Loading
    </b>
    <div class="fp_switch"></div>
  </div>
  <div id="fp_verbaige">
    <a href="/">
      <div class="gallery_fplink">
        Problems with the gallery?<br />
        This default link will send you home
      </div>
    </a>
  </div>
  <div class="fp_panel">
    <div id="fp-slider"></div>
    <div id="fp-slider-value"></div>
  </div>
  <p>
    <i>*Full Screen Gallery requires a JavaScript capable browser</i>
    <p>
      <p class="fp_instruct"><u>Click to Show Instructions&nbsp;&nbsp;---&nbsp;&nbsp;Or to start, select a thumbnail from below.</u></p>
      <div id="fp_instructions">
        <ul>
          <li>Navigate using the thumbnails located at the bottom of the screen</li>
          <li>Hover the mouse and click over the image you want to view.  On a touch screen you can use touch to scroll left or right before selecting a photo.</li>
          <li>When a photo is loaded you may use the left and right navigation arrows, or again navigate with the thumbnails, to change photos.</li>
          <li>If you leave the mouse hovering over the next or previous navigation arrows, after 5 seconds the slideshow starts.</li>
          <li>To hide the full screen view and leave, type the ESC key, or click outside the photo window.</li>
          <li>Touchscreen devices highlight the next/previous buttons when clicked, this will eventually start the slideshow.  Click on the picture (not outside the picture) to prevent the slideshow from starting after using the next/previous button.</li>
        </ul>
      </div>
      <br />
      <!--  put an image here if you like, just to push the thumbnails down -->
      <div class="fp_emptyspace"></div>
      <!--  the div fp_emptyspace can be removed -->
    </p>
  </p>
</div>
<div id="fp_modpath"><?php print $variables['module']['path']; ?></div>
<div id="fp_gallery" class="fp_gallery noJS-fp">
  <div id="fp_exit"><h2>Press ESC key or click outside photo to hide full screen window.</h2></div>
  <b><span class="helper"></span>
    <img src="/<?php print $variables['module']['path']; ?>/res/transdot.png" alt="" id="preview" class="fp_preview" style="" />
  </b>
  <div id="fp_caption"><h3></h3></div>
  <div id="fp_loading" class="fp_loading"></div>
  <div id="fp_next" class="fp_next"></div>
  <div id="fp_prev" class="fp_prev"></div>
  <div id="outer_container" class="noJS-fp">
    <div id="thumbScroller" class="noJS-fp">
      <div class="thumb-content">
<?php
foreach ($rows as $row_count => $row):
  foreach ($row as $field => $content):
    print '<div class="fp-content">' . $content . '</div>';
  endforeach;
endforeach;
?>
      </div>
    </div>
    <div id="fp_thumbtoggle" class="fp_thumbtoggle">View Thumbs</div>
  </div>
  <div class="thumbrowspace"></div>
</div>
